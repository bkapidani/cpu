#ifndef PROGRAM_COUNTER_HPP
#define PROGRAM_COUNTER_HPP

//In this header file, we'll only describe the central program counter block
//(not the multiplexer as well, which will be described in another file).
//Thus, the architecture consists only in one 16-bit input, which comes out
//from a three-input multiplexer, and an output, which is exactly the same as
//the input: the program counter is just a register, it's the surrounding architecture
//that could be more complicated

#include <systemc.h>
#include <iostream>

SC_MODULE(Program_Counter){

	//Declaration of the inputs
	sc_in<bool> clock;
	sc_in<sc_bv<16> > in_addr;
	sc_in<bool> control_ready;

	//Declaration of the outputs
	sc_out<sc_bv<16> > out_addr;

	//Declaration of internal variables
	int lock;

	//Methods Declaration
	void ReadAddr();

	//Constructor
	SC_CTOR(Program_Counter) {

			SC_METHOD(ReadAddr);
			sensitive << clock.pos();
			dont_initialize();
	}	

};//End of the Program Counter Module

#endif
