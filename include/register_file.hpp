#ifndef REGISTER_FILE_HPP
#define REGISTER_FILE_HPP

//According to the given example, in the Register File there are eight registers,
//each of them containing a 16-bit word. Since the address coming in the port TGT
//is a three-bit word, we're addressing to the registers and not to the single 
//location itself. For this reason, we define a MEMORY SIZE equal to 8. At the same
//time, we define a flag noticing whether the given address is valid or not.

#define MEMORY_SIZE 8
#define INVALID_ADDRESS 1

#include <systemc.h>
#include <iostream>

SC_MODULE(Register_File){

//For the structure of the Register File, please refer to the document "Risc-16 
//Sequential Implementation", that can be found at http://www.eng.umd.edu/~blj/RiSC/.
//The structure we are going to implement can be easily deduced from the one reported
//at pag.10.

	//Declaration of the inputs
	sc_in<bool> clock;
	sc_in<sc_bv<3> > tgt_addr;
	sc_in<sc_bv<3> > src1_addr;
	sc_in<sc_bv<3> > src2_addr;
	sc_in<sc_bv<16> > tgt;
	sc_in<sc_bv<1> > we;
	sc_in<bool> control_ready;

	//Declaration of the outputs
	sc_out<bool> register_file_ready;
	sc_out<sc_bv<16> > src1;
	sc_out<sc_bv<16> > src2;


	//Declaration of signals having testing function
	sc_bv<16> MemData[MEMORY_SIZE];
	sc_bv<3> src1_addr_bv;
	int src1_addr_int;
	sc_bv<3> src2_addr_bv;
	int src2_addr_int;
	sc_bv<3> tgt_addr_bv;
	int tgt_addr_int;
//	int lock;
//	int spiete;

	//Methods Declaration
	void ReadSrc1();
	void ReadSrc2();
	void Write();
	void Execute();
	void GetIntFromBitVectorSrc1();
	void GetIntFromBitVectorSrc2();
	void GetIntFromBitVectorTgt();
	void GetValues();

	//Constructor
	SC_CTOR(Register_File)
	{

		for (int i=0; i<MEMORY_SIZE; i++) 
			MemData[i]="0000000000000000";

		int src1_addr_int = 0;
		int src2_addr_int = 0;
		int tgt_addr_int = 0;
//		int lock = 0;

		SC_METHOD(Execute);
			sensitive << clock.pos() << src1_addr << src2_addr;
			dont_initialize();
	}	

};//End of the Register File Module

#endif
