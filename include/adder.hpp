#ifndef ADDER_HPP
#define ADDER_HPP

#include <systemc.h>

using namespace std;

SC_MODULE(Adder)          // module declaration
{
   sc_in<sc_bv<16> > a;
   sc_in<sc_bv<16> > b;
   sc_out<sc_bv<16> > s;

    // methods
    void add();
    
    //Constructor
    SC_CTOR( Adder ) {

	SC_METHOD(add); 
 	sensitive << a << b;
	dont_initialize();
    }
};

#endif

