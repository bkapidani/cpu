#ifndef T_REGISTER_HPP
#define T_REGISTER_HPP

#include <systemc.h>

using namespace std;

template <class T>
SC_MODULE(T_Register)          // module declaration
{

   sc_in<bool> clock;   
   sc_in<T> in1;
   sc_out<T> out1;


   // methods
   void route();
    
   //Constructor
   SC_CTOR(T_Register) {

	SC_METHOD(route); 
 	sensitive << clock.pos();
   }

};

template <class T>
void T_Register<T> :: route()
{
   T router;
   router=in1.read();

   out1.write(router);
}


#endif

