#include <systemc.h>

SC_MODULE(Interface_Stimulus){

	//Declaration of the outputs
	sc_out<sc_bv<1> > eq;	//eq output to conttrol
	sc_out<sc_bv<3> > op_code;	//operation code to control
	sc_out<sc_bv<1> > load;	//load to control
	sc_out<sc_bv<3> > r_a; //register addresses to register_file
	sc_out<sc_bv<3> > r_b;
	sc_out<sc_bv<3> > tgt_addr;
	sc_out<sc_bv<16> > data_mem_out; // outputs to the mux for tgt of the register file
	sc_out<sc_bv<16> > alu_out;
	sc_out<sc_bv<16> > sign_ext7;	//16-bit extension of constant field of instruction
	sc_out<sc_bv<16> > one;		//It will be declared as "0000000000000001"

	//Declaration of the inputs
	sc_in<bool> clock;
	sc_in<sc_bv<1> > mux_alu1;
	sc_in<sc_bv<1> > mux_alu2;
	sc_in<sc_bv<2> > func_alu;
	sc_in<sc_bv<1> > we_dmem;
	sc_in<sc_bv<1> > re_dmem;
	sc_in<sc_bv<16> > src1;
	sc_in<sc_bv<16> > src2;

	void stimgen();

	SC_CTOR(Interface_Stimulus){

		SC_THREAD(stimgen);
		sensitive << clock.pos();	

	}

};
