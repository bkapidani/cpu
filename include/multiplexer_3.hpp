#ifndef MUX_3_HPP
#define MUX_3_HPP

#include <systemc.h>

using namespace std;

SC_MODULE(Multiplexer_3)
{
	//sc_in<bool> clock;
   	sc_in<sc_bv<16> > in1;
   	sc_in<sc_bv<16> > in2;
   	sc_in<sc_bv<16> > in3;
   	sc_in<sc_bv<2> > sel;
   	sc_out<sc_bv<16> > result;

   	void do_selection();

   	SC_CTOR(Multiplexer_3) {
      		SC_METHOD(do_selection); 
      		sensitive << sel << in1 << in2 << in3; 
		dont_initialize();
   	}

};

#endif
