#include "control_unit.hpp"

using namespace std;

//Decoding Function
void Control_Unit::Decode(){
	
	cout << "Control Unit @ " << sc_time_stamp() << endl;
	
	control_ready = 0;
	op_code_int = op_code.read();

	if (op_code_int[2] == '0')
	{

		if (op_code_int[1] == '0') 
		{

			if (op_code_int[0] == '0') 
			{
				if (beq_check==true && eq.read() == "1") //This is a NOP instruction required for branch prediction
				{				
					cout << "	The incoming opcode corresponds to a NOP operation prior to branching" << endl;
					
					//op_code 000: NOP
					mux_pc = "01";	//Assuming that the 01 code stays for PC+1+imm-7
					cout << "	mux_pc value is " << mux_pc << endl;
					cout << "	mux_pc selects the pc+1+sign7 as next address for PC" << endl;	

					mux_alu1 = "0";	//Passing src1 to the ALU
					mux_alu2 = "0";	//Passing src2 to the ALU 

					func_alu = "01";	//ALU function ADD corresponds to the 01 code

					we_dmem = "0";	//No writing enable for the data memory
					re_dmem = "0";	//Reading enable for the data memory
					we_rf = "1";	//Writing enable for the register file
					cout << "	Write Enable has been set for the register file" << endl;

					mux_tgt = "00";	//The TGT coming out from the ALU corresponds to 00
					cout << "	mux_tgt selects alu_out as tgt incoming data for the reg. file" << endl;

					mux_rf = "0";	//Redirecting rC to src2 corresponds to 0
					cout << "	mux_rf selects r_c as source 2 address for the register file" << endl;
					//beq_check=false;
				}
				else if (jalr_check==true)
				{				
					cout << "	The incoming opcode corresponds to a NOP operation for function return" << endl;

					//op_code 000: ADD
					mux_pc = "10";	//Assuming that the 10 code stays for taking
									//to the PC the output of the ALU

					mux_alu1 = "0";	//Passing src1 to the ALU
					mux_alu2 = "0";	//Passing src2 to the ALU 

					func_alu = "01";	//ALU function ADD corresponds to the 01 code

					we_dmem = "0";	//No writing enable for the data memory
					re_dmem = "0";	//Reading enable for the data memory
					we_rf = "1";	//Writing enable for the register file
					cout << "	Write Enable has been set for the register file" << endl;

					mux_tgt = "00";	//The TGT coming out from the ALU corresponds to 00
					cout << "	mux_tgt selects the alu_output as tgt incoming data for the register file" << endl;

					mux_rf = "0";	//Redirecting rC to src2 corresponds to 0
					cout << "	mux_rf selects r_c as source 2 address for the register file" << endl;
					//beq_check=false;
	
				}
				else
				{				
					cout << "	The incoming opcode corresponds to the function ADD" << endl;

					//op_code 000: ADD
					mux_pc = "00";	//Assuming that the 00 code stays for PC+1	

					mux_alu1 = "0";	//Passing src1 to the ALU
					mux_alu2 = "0";	//Passing src2 to the ALU 

					func_alu = "01";	//ALU function ADD corresponds to the 01 code

					we_dmem = "0";	//No writing enable for the data memory
					re_dmem = "0";	//Reading enable for the data memory
					we_rf = "1";	//Writing enable for the register file
					cout << "	Write Enable has been set for the register file" << endl;

					mux_tgt = "00";	//The TGT coming out from the ALU corresponds to 00
					cout << "	mux_tgt selects the alu_output as tgt incoming data for the register file" << endl;

					mux_rf = "0";	//Redirecting rC to src2 corresponds to 0
					cout << "	mux_rf selects r_c as source 2 address for the register file" << endl;
					//beq_check=false;
					
	
				}			
				
				
			}
			else
			{

				cout << "	The incoming opcode corresponds to the function ADDI" << endl;

				//op_code 001: ADDI
				mux_pc = "00";	//Assuming that the 00 code stays for PC+1

				mux_alu1 = "0";	//Passing src1 to the ALU
				mux_alu2 = "1";	//Passing Sign-Extend-7 to the ALU 

				func_alu = "01";	//ALU function ADD corresponds to the 01 code

				we_dmem = "0";	//No writing enable for the data memory
				re_dmem = "0";	//Reading enable for the data memory
				we_rf = "1";	//Writing enable for the register file
				cout << "	Write Enable has been set for the register file" << endl;

				mux_tgt = "00";	//The TGT coming out from the ALU corresponds to 00
				cout << "	mux_tgt selects the alu_output as target incoming data for the register file" << endl;

				mux_rf = "1";
				cout << "	mux_rf selects r_a as source 2 address for the register file" << endl;
						//Redirecting rA to src2 corresponds to 1
						//Actually in this operation it doesn't matter which register			
						//are we passing to src2, since it will be ignored thanks to the 
						//mux_alu2 signal.
				
				beq_check=false;
				jalr_check=false;
			}

		} 
		else 
		{

			if (op_code_int[0] == '0') 
			{


				cout << "	The incoming opcode corresponds to the function NAND" << endl;

				//op_code 010: NAND
				mux_pc = "00";	//Assuming that the 00 code stays for PC+1	

				mux_alu1 = "0";	//Passing src1 to the ALU
				mux_alu2 = "0";	//Passing src2 to the ALU 

				func_alu = "11";	//ALU function NAND corresponds to the 10 code

				we_dmem = "0";	//No writing enable for the data memory
				re_dmem = "0";	//Reading enable for the data memory
				we_rf = "1";	//Writing enable for the register file
				cout << "	Write Enable has been set for the register file" << endl;

				mux_tgt = "00";	//The TGT coming out from the ALU corresponds to 00
				cout << "	mux_tgt selects the alu_output as target incoming data for the register file" << endl;

				mux_rf = "0";	//Redirecting rC to src2 corresponds to 0
				cout << "	mux_rf selects r_c as source 2 address for the register file" << endl;
				
				beq_check=false;
				jalr_check=false;		
			} 
			else
			{

				cout << "	The incoming opcode corresponds to the function LUI" << endl;

				//op_code 011: LUI
				mux_pc = "00";	//Assuming that the 00 code stays for PC+1	

				mux_alu1 = "1";	//Passing Left-Shift-6 to the ALU
				mux_alu2 = "0";	//Passing src2 to the ALU . Here I could actually pass
						//to the ALU whatever I like, since it must be ignored
						//when the ALU is in PASS1 mode.

				func_alu = "00";	//ALU function PASS1 corresponds to the 00 code

				we_dmem = "0";	//No writing enable for the data memory
				re_dmem = "1";	//Reading enable for the data memory
				we_rf = "1";	//Writing enable for the register file
				cout << "	Write Enable has been set for the register file" << endl;

				mux_tgt = "00";	//The TGT coming out from the ALU corresponds to 00
				cout << "	mux_tgt selects the alu_output as target incoming data for the register file" << endl;

				mux_rf = "0";
				cout << "	mux_rf selects r_c as source 2 address for the register file" << endl;
						//Redirecting rC to src2 corresponds to 0. I could set 
						//this bit however I like, since src2 will be ignored due
						//to the fact that the ALU is in pass mode.

				beq_check=false;
				jalr_check=false;
			}
		
		}
	} 
	else 
	{
	
		if (op_code_int[1] == '0') 
		{

			if (op_code_int[0] == '0') 
			{
		
				cout << "	The incoming opcode corresponds to the function SW" << endl;

				//op_code 100: SW
				mux_pc = "00";	//Assuming that the 00 code stays for PC+1	

				mux_alu1 = "0";	//Passing src1 to the ALU
				mux_alu2 = "1";	//Passing Sign-Extend-7 to the ALU 

				func_alu = "01";	//ALU function ADD corresponds to the 01 code

				we_dmem = "1";	//Writing enable for the data memory
				re_dmem = "0";	//No reading enable for the data memory
				we_rf = "0";	//No writing enable for the register file
				cout << "	Write Enable has been not set for the register file" << endl;

				mux_tgt = "01";	//The Data Memory output corresponds to 01
				cout << "	mux_tgt selects the data_memory_output as target input data" << endl;

				mux_rf = "1";	
				cout << "	mux_rf selects r_a as source 2 address for the register file" << endl;
						//Redirecting rA to src2 corresponds to 1
				
				beq_check=false;
				jalr_check=false;				
			} 
			else
			{
				cout << "	The incoming opcode corresponds to the function LW" << endl;

				//op_code 101: LW
				mux_pc = "00";	//Assuming that the 00 code stays for PC+1

				mux_alu1 = "0";	//Passing src1 to the ALU
				mux_alu2 = "1";	//Passing Sign-Extend-7 to the ALU 

				func_alu = "01";	//ALU function ADD corresponds to the 01 code

				we_dmem = "0";	//No writing enable for the data memory
				re_dmem = "1";	//Reading enable for the data memory
				we_rf = "1";	//Writing enable for the register file
				cout << "	Write Enable has been set for the register file" << endl;

				mux_tgt = "01";	//The Data Memory output corresponds to 01
				cout << "	mux_tgt selects the data_memory_output as target input data" << endl;

				mux_rf = "1";	
				cout << "	mux_rf selects r_a as source 2 address for the register file" << endl;
						//Redirecting rA to src2 corresponds to 1
						//Actually in this operation it doesn't matter which register			
						//are we passing to src2, since it will be ignored thanks to the 
						//mux_alu2 signal.

				beq_check=false;
				jalr_check=false;				

			}

		}
		else
		{

			if (op_code_int[0] == '0') 
			{

				cout << "	The incoming opcode corresponds to the function BEQ" << endl;

				//op_code 110: BEQ
				//we will use the MIPS convention: the instruction after the branch is always executed
				//We will use a NOP (static branch prediction: not taken)
				
				
				mux_pc = "00";	//Assuming that the 00 code stays for PC+1 which is a NOP

				mux_alu1 = "0";	//Passing src1 to the ALU
				mux_alu2 = "0";	//Passing src2 to the ALU 

				func_alu = "00";	//ALU function PASS1 corresponds to the 00 code

				we_dmem = "0";	//No writing enable for the data memory
				re_dmem = "0";	//Reading enable for the data memory
				we_rf = "0";	//No writing enable for the register file
				cout << "	Write Enable has been not set for the register file" << endl;

				mux_tgt = "00";	//The TGT coming out from the ALU corresponds to 00
				cout << "	mux_tgt selects the alu_output as target incoming data for the register file" << endl;

				mux_rf = "1";	
				cout << "	mux_rf selects r_a as source 2 address for the register file" << endl;
						//Redirecting rA to src2 corresponds to 0

				beq_check=true;	//allows the NOP to evaluate condition
				jalr_check=false;
			}
			else
			{

				cout << "	The incoming opcode corresponds to the function JALR" << endl;

				//op_code 111: JALR
				mux_pc = "00";	//Assuming that the 00 code stays for PC+1	

				mux_alu1 = "0";	//Passing src1 to the ALU
				mux_alu2 = "0";	//Passing src2 to the ALU 

				func_alu = "00";	//ALU function PASS1 corresponds to the 00 code

				we_dmem = "0";	//No writing enable for the data memory
				re_dmem = "1";	//Reading enable for the data memory
				we_rf = "1";	//Writing enable for the register file
				cout << "	Write Enable has been set for the register file" << endl;

				mux_tgt = "10";	//The PC output corresponds to 10
				cout << "	mux_tgt selects the pc_output as target incoming data for the register file" << endl;

				mux_rf = "0";	
				cout << "	mux_rf selects r_c as source 2 address for the register file" << endl;
						//Redirecting rC to src2 corresponds to 0. I could set 
						//this bit however I like, since src2 will be ignored due
						//to the fact that the ALU is in pass mode.
				
				beq_check=false;
				jalr_check=true;
			}
		
		}

	}
	
	control_ready = '1';

}

