// multiplexer

#include "multiplexer_3.hpp"

using namespace std;


void Multiplexer_3::do_selection()
{
	sc_bv<16> temp;
   	sc_bv<2> sel_signal = sel.read();
	cout << "Target Data multiplexer @ " << sc_time_stamp() << endl;
	cout << "	Input sel_signal = " << sel_signal << "(00 = alu_output, 01 = data memory, 10 = pc_plus_one)" << endl;

   	if( sel_signal == "00" )
   	{
      		temp = in2.read();
		cout << "	alu_output selected as target data for the register file" << endl;
   	}
   	else if ( sel_signal == "01" )
   	{
      		temp = in1.read();
		cout << "	data memory output selected as target data for the register file" << endl;
   	}
   	else
   	{
      		temp = in3.read();
		cout << "	pc_plus_one selected as target data for the register file" << endl;
   	}

   	result.write(temp);
}
