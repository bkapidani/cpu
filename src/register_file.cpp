#include "register_file.hpp"

using namespace std;

void Register_File::GetValues()
{
	src1_addr_bv = src1_addr.read();
	src2_addr_bv = src2_addr.read();
	tgt_addr_bv = tgt_addr.read();
	
	return;
}

//Conversion Function
void Register_File::GetIntFromBitVectorSrc1()
{		
	if (src1_addr_bv[2]=='0')
	{
		if (src1_addr_bv[1] == '0')
		{
			if (src1_addr_bv[0] == '0')
			{
				src1_addr_int = 0;
			} else {
				src1_addr_int = 1;
			}
		} else {
			if (src1_addr_bv[0] == '0')
			{
				src1_addr_int = 2;
			} else {
				src1_addr_int = 3;
			}
		}
	} else {
		if (src1_addr_bv[1] == '0')
		{
			if (src1_addr_bv[0] == '0')
			{
				src1_addr_int = 4;
			} else {
				src1_addr_int = 5;
			}
		} else {
			if (src1_addr_bv[0] == '0')
			{
				src1_addr_int = 6;
			} else {
				src1_addr_int = 7;
			}
		}
	}


	return;
}


void Register_File::GetIntFromBitVectorSrc2()
{	
	if (src2_addr_bv[2] == '0')
	{
		if (src2_addr_bv[1] == '0')
		{
			if (src2_addr_bv[0] == '0')
			{
				src2_addr_int = 0;
			} else {
				src2_addr_int = 1;
			}
		} else {
			if (src2_addr_bv[0] == '0')
			{
				src2_addr_int = 2;
			} else {
				src2_addr_int = 3;
			}
		}
	} else {
		if (src2_addr_bv[1] == '0')
		{
			if (src2_addr_bv[0] == '0')
			{
				src2_addr_int = 4;
			} else {
				src2_addr_int = 5;
			}
		} else {
			if (src2_addr_bv[0] == '0')
			{
				src2_addr_int = 6;
			} else {
				src2_addr_int = 7;
			}
		}
	}


	return;
}


void Register_File::GetIntFromBitVectorTgt()
{
	if (tgt_addr_bv[2] == '0')
	{
		if (tgt_addr_bv[1] == '0')
		{
			if (tgt_addr_bv[0] == '0')
			{
				tgt_addr_int = 0;
			} else {
				tgt_addr_int = 1;
			}
		} else {
			if (tgt_addr_bv[0] == '0')
			{
				tgt_addr_int = 2;
			} else {
				tgt_addr_int = 3;
			}
		}
	} else {
		if (tgt_addr_bv[1] == '0')
		{
			if (tgt_addr_bv[0] == '0')
			{
				tgt_addr_int = 4;
			} else {
				tgt_addr_int = 5;
			}
		} else {
			if (tgt_addr_bv[0] == '0')
			{
				tgt_addr_int = 6;
			} else {
				tgt_addr_int = 7;
			}
		}
	}


	return;
}

//Read function
void Register_File::ReadSrc1()
{	
	src1.write(MemData[src1_addr_int]);

	return;
}

//Read function
void Register_File::ReadSrc2()
{
	GetIntFromBitVectorSrc2();
	src2.write(MemData[src2_addr_int]);

	return;
}

//Write function
void Register_File::Write()
{
	GetIntFromBitVectorTgt();
	MemData[tgt_addr_int] = tgt.read();

	return;
}

//Execution body
void Register_File::Execute()
{
	cout << "Register File @ " << sc_time_stamp() << endl;

	if(we.read()==1)
	{
	//If the write enable bit is high, then the data 			
	//in src1_addr and src2_addr must be brought to the 			
	//outputs src1 and src2. At the same time, the
	//result of the operation carried out, present 				
	//at the input port TGT, must be written in the 			
	//tgt_address.

		GetValues();
		cout << "	Il valore di src1_addr_bv e' " << src1_addr_bv << endl;
		cout << "	Il valore di src2_addr_bv e' " << src2_addr_bv << endl;
		cout << "	Il valore di tgt_addr_bv e' " << tgt_addr_bv << endl;
		GetIntFromBitVectorSrc1();
		cout << "	Starting the writing process..." << endl;

		ReadSrc1();
		ReadSrc2();
		Write();

		if (register_file_ready) 
		{

			cout << "	Source 1 Data = " << src1 << endl;
			cout << "	Source 2 Data = " << src2 << endl;
			cout << "	Target Data " << tgt << endl;

			register_file_ready = 0;
	
		} 
		else
		{

			cout << "	Preparing outputs..."<< endl;

		}


		cout << "	Writing process finished" << endl;
	} 
	else 
	{
		GetValues();
		GetIntFromBitVectorSrc1();
		cout << "	Il valore di src1_addr_bv e' " << src1_addr_bv << endl;
		cout << "	Il valore di src1_addr_int e' " << src1_addr_int << endl;
		cout << "	Il valore di src2_addr_bv e' " << src2_addr_bv << endl;
		cout << "	Il valore di tgt_addr_bv e' " << tgt_addr_bv << endl;
		cout << "	Starting the reading process..." << endl;

		ReadSrc1();
		ReadSrc2();

		if (register_file_ready) 
		{

			cout << "	Source 1 Data = " << src1 << endl;
			cout << "	Source 2 Data = " << src2 << endl;

			register_file_ready = 0;
	
		} 
		else
		{

			cout << "	Preparing outputs...";

		}

		cout << "	Reading process finished" << endl;
	}
}

