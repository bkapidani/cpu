// multiplexer

#include "multiplexer_3_2.hpp"

using namespace std;


void Multiplexer_3_2::do_selection()
{
	sc_bv<16> temp;
   	sc_bv<2> sel_signal = sel.read();
	cout << "Program Counter multiplexer @ " << sc_time_stamp() << endl;
	cout << "	Input sel_signal = " << sel_signal << "(00 = pc+1, 01 = pc+1+sign_ext7, 10 = alu_output)" << endl;

   	if( sel_signal == "00" )
   	{
      		temp = in2.read();
		cout << "	pc+1 selected as target data for the register file" << endl;
   	}
   	else if ( sel_signal == "01" )
   	{
      		temp = in1.read();
		cout << "	pc+1 augmented by sign_ext7 selected as target data for the register file" << endl;
   	}
   	else
   	{
      		temp = in3.read();
		cout << "	alu output selected as target data for the register file" << endl;
   	}

   	result.write(temp);
}
