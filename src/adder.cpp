#include <systemc.h>
#include "adder.hpp"

using namespace std;

void Adder::add()
{
	s.write(a.read().to_uint() + b.read().to_uint());

	cout << "Input a of the adder is " << a << endl;
	cout << "Input b of the adder is " << b << endl;	
}

